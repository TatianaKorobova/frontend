import React from 'react';

import Countries from './pages/Countries/Countries'
import WorldMap  from './pages/Map/WorldMap'
import Homepage  from './pages/Homepage/Homepage'

let routes = [
  {
    name:        'home',
    displayName: 'Homepage',
    url:         '/',
    component:   Homepage,
    exact:       true,
  },
  {
    name:        'countries',
    displayName: 'Countries',
    url:         '/countries',
    routes:      ['/countries', '/countries/:page'],
    component:   Countries,
    exact:       true,
  },
  {
    name:        'world-map',
    displayName: 'World Map',
    url:         '/world-map',
    component:   WorldMap,
    exact:       true,
  }
]

export default routes;