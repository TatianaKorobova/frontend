import React, { useEffect, useState } from 'react';

import './worldMap.scss';
import Map                            from "../../components/Map/Map";
import api                            from "../../api/api";

function WorldMap() {
  const [countries, setCountries]             = useState([]);
  const [selectedCountry, setSelectedCountry] = useState(null);

  useEffect(async () => {
    const {rows} = await api.getCountries();
    setCountries(rows);
  }, [])

  return (
    <section className="world-map">
      <h1 className="world-map__title">Map</h1>
      <Map countries={countries}
           selectedCountry={selectedCountry}
           setSelectedCountry={setSelectedCountry}
      />
    </section>
  );
}

export default WorldMap;