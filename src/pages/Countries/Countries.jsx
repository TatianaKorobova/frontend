import React, { useState, useEffect } from 'react';

import CountriesList from "../../components/CountriesList/CountriesList";
import SearchBox     from "../../components/SearchBox/SearchBox";
import EditPopup     from "../../components/EditPopup/EditPopup";

import api from '../../api/api';

import './countries.scss';

function Countries({history, match}) {
  let debounce = null;

  const [countries, setCountries]             = useState({list: [], count: 0});
  const [pagination, setPagination]           = useState({page: 0, rows: 5});
  const [selectedCountry, setSelectedCountry] = useState(null);
  const [search, setSearch]                   = useState({searchValue: '', searchProp: 'name'});

  useEffect(() => {
    const page = (+match.params.page && !Number.isNaN(+match.params.page)
    )
      ? +match.params.page : 0;
    setPagination({...pagination, page})
  }, [])

  useEffect(() => {
    debounce = setTimeout(async () => {
      const countries = await api.getCountries(pagination, search);
      if (countries) {
        setCountries({...countries, count: countries.count, list: countries.rows})
      }
    }, 500);

    return () => {
      clearTimeout(debounce);
    }
  }, [pagination, search])

  const handleChangePage = (_, page) => {
    setPagination({...pagination, page})
    history.push(`/countries/${page}`)
  }

  const handleChangeRows = ({target}) => {
    setPagination({page: 0, rows: target.value})
  }

  const handleChangeSearch = ({target}) => {
    const {name, value} = target;
    setSearch({...search, [name]: value})
  }

  const deleteCountry = async (countryCode) => {
    const countries = await api.deleteCountry(countryCode, pagination, search);
    if (countries) {
      setCountries({...countries, count: countries.count, list: countries.rows})
    }
  }

  const updateCountry = async (country) => {
    const updatedCountry = await api.updateCountry(country, pagination, search);

    if (updatedCountry) {
      const updatedCountriesList = JSON.parse(JSON.stringify(countries.list));
      const updatedCountryIndex = updatedCountriesList.findIndex((country) => {
        return updatedCountry.code === country.code
      });
      updatedCountriesList.splice(updatedCountryIndex, 1, updatedCountry)
      setCountries({...countries, list: updatedCountriesList})

      setSelectedCountry(null);
    }
  }

  return (
    <section className="countries">
      <div className="countries__header">
        <h1 className="countries__title">Countries</h1>
        <SearchBox
          search={search}
          setSearch={handleChangeSearch}
        />
      </div>
      <CountriesList
        countries={countries}
        pagination={pagination}
        handleChangePage={handleChangePage}
        handleChangeRows={handleChangeRows}
        setCountryForEditing={setSelectedCountry}
        deleteCountry={deleteCountry}
      />
      {selectedCountry && (
        <EditPopup
          selectedCountry={selectedCountry}
          setSelectedCountry={setSelectedCountry}
          updateCountry={updateCountry}
        />
      )
      }
    </section>
  );
}

export default Countries;