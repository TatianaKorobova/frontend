import React   from 'react';

import './homepage.scss';

function Homepage() {
  return (
    <div>
      Welcome to World Map CMS!
    </div>
  );
}

export default Homepage;