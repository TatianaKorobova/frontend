const BASE_URL = process.env.REACT_APP_BASE_URL;

const getCountriesUrl = (path, pagination, search) => {
  const url = new URL(`${BASE_URL}/${path}`);

  if (pagination !== undefined) {
    url.searchParams.set('page', pagination.page);
    url.searchParams.set('rows', pagination.rows);
  }

  if (search !== undefined) {
    url.searchParams.set('searchValue', search.searchValue);
    url.searchParams.set('searchProp', search.searchProp);
  }

  return url;
}

const api = {
  getCountries:  async (pagination, search) => {
    try {
      const url    = getCountriesUrl('countries', pagination, search);
      const result = await fetch(url);
      if (result && result.ok) {
        return result.json()
      } else {
        throw result;
      }
    } catch (error) {
      alert('Something went wrong')
      console.log(error);
    }
  },
  deleteCountry: async (countryCode, pagination, search) => {
    try {
      const url    = getCountriesUrl(`countries/${countryCode}`, pagination, search);
      const result = await fetch(url, {method: 'DELETE'});
      if (result && result.ok) {
        return result.json()
      } else {
        throw result;
      }
    } catch (error) {
      alert('Something went wrong')
      console.log(error)
    }
  },
  updateCountry: async (country, pagination, search) => {
    try {
      const url    = getCountriesUrl('countries/', pagination, search);
      await fetch(url, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({country})
      });

      return country;
    } catch (error) {
      alert('Something went wrong')
      console.log(error)
    }
  }
};

export default api;