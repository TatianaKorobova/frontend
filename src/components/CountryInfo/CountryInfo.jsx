import React          from "react";
import { InfoWindow } from "@react-google-maps/api";

const CountryInfo = ({selectedCountry, closeInfo}) => {
  const [lat, lng] = selectedCountry.latlng;

  return (
    <InfoWindow
      position={{lat: lat + 3, lng}}
      onCloseClick={closeInfo}
    >
      <>
        <p>{selectedCountry.code}</p>
        <p>{selectedCountry.name}</p>
        <p>Coords: {`${lat} ${lng}`}</p>
        <p>Description: {selectedCountry.description}</p>
      </>
    </InfoWindow>
  )
};

export default CountryInfo;