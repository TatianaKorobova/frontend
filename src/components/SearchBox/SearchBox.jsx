import React            from "react";
import Radio            from '@mui/material/Radio';
import RadioGroup       from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl      from '@mui/material/FormControl';
import FormLabel        from '@mui/material/FormLabel';
import TextField        from '@mui/material/TextField';

import "./searchBox.scss";

const SearchBox = ({search, setSearch}) => (
  <div className="search-box">
    <TextField
      name="searchValue"
      value={search.searchValue}
      onChange={setSearch}
      label="Search"
    />
    <FormControl component="fieldset">
      <FormLabel>Search by:</FormLabel>
      <RadioGroup
        name="searchProp"
        value={search.searchProp}
        onChange={setSearch}
        label="Search"
        className="search-box__radio"
      >
        <FormControlLabel value="name" control={<Radio/>} label="Name"/>
        <FormControlLabel value="code" control={<Radio/>} label="Code"/>
      </RadioGroup>
    </FormControl>
  </div>
);

export default SearchBox;