import React, { useState } from 'react';
import Box                 from '@mui/material/Box';
import SwipeableDrawer     from '@mui/material/SwipeableDrawer';
import Button              from '@mui/material/Button';
import List                from '@mui/material/List';
import ListItem            from '@mui/material/ListItem';
import MenuIcon            from '@mui/icons-material/Menu';
import { NavLink }         from "react-router-dom";

import routes from "../../routes";

import './sidebar.scss';

const Sidebar = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleDrawer = (open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift'
    )) {
      return;
    }

    setIsOpen(open);
  };

  return (
    <div className="sidebar">
      <Button onClick={toggleDrawer(true)}>
        <MenuIcon/>
      </Button>
      <NavLink className="sidebar__logo" to="/">CMS World Map</NavLink>
      <SwipeableDrawer
        open={isOpen}
        onOpen={toggleDrawer(true)}
        onClose={toggleDrawer(false)}
      >
        <Box
          sx={{width: 250}}
          role="presentation"
          onClick={toggleDrawer(false)}
          onKeyDown={toggleDrawer(false)}
        >
          <List>
            {routes.map(({displayName, url, name}, index) => (
              <ListItem button key={name}>
                <NavLink to={url} activeClassName="active">
                  {displayName}
                </NavLink>
              </ListItem>
            ))}
          </List>
        </Box>
      </SwipeableDrawer>
    </div>
  );
}

export default Sidebar;