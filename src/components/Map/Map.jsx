import React                                from "react";
import { Marker, GoogleMap, useLoadScript } from "@react-google-maps/api";

import CountryInfo from "../CountryInfo/CountryInfo";

const libraries         = ['places'];
const center            = {lat: 40, lng: 10};
const mapContainerStyle = {width: '100%', height: '100%'}

const Map = ({countries, selectedCountry, setSelectedCountry}) => {
  const {isLoaded} = useLoadScript({
    googleMapsApiKey: process.env.REACT_APP_GOOGLE_MAP_API,
    libraries
  });

  return isLoaded ? (
    <GoogleMap
      id='world-map'
      zoom={2}
      mapContainerStyle={mapContainerStyle}
      center={center}
    >
      {
        countries.map(country => {
          const [lat, lng] = country.latlng;
          return (
            <Marker
              key={country.code}
              position={{lat, lng}}
              icon={`${process.env.REACT_APP_FLAGS_PATH}/24/${country.code.toLowerCase()}.png`}
              onClick={() => setSelectedCountry(country)}
            />
          )
        })
      }
      {selectedCountry && (
        <CountryInfo
          selectedCountry={selectedCountry}
          closeInfo={() => setSelectedCountry(null)}
        />
      )}
    </GoogleMap>
  ) : <></>

};

export default Map;