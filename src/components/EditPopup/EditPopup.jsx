import React, { useState } from "react";
import TextField           from '@mui/material/TextField';
import Button              from '@mui/material/Button';
import Dialog              from '@mui/material/Dialog';
import DialogContent       from '@mui/material/DialogContent';
import DialogTitle         from '@mui/material/DialogTitle';

import './edit-popup.scss';

const EditPopup = ({selectedCountry, setSelectedCountry, updateCountry}) => {
  const [updatedCountry, setUpdatedCountry] = useState(selectedCountry);

  const handleClose = () => {
    setSelectedCountry(null);
  };

  const submitEditForm = (event) => {
    event.preventDefault();
    updateCountry(updatedCountry);
  }

  const handleChange = ({target}) => {
    const {name, value} = target;
    let updates = {};

    if (name === 'lat' || name === 'lng') {
      const latlng = updatedCountry.latlng;
      const index = name === 'lat' ? 0 : 1
      latlng[index] = +value;
      updates = ({latlng})
    } else {
      updates = {[name]: value}
    }

    setUpdatedCountry({...updatedCountry, ...updates})
  }

  return (
    <Dialog open={!!selectedCountry} onClose={handleClose} className="edit-popup">
      <DialogTitle>Edit country</DialogTitle>
      <DialogContent>
        <form onSubmit={submitEditForm} className="edit-popup__form">
          <TextField
            name="code"
            disabled
            value={updatedCountry.code}
            onChange={handleChange}
            label="Code"
          />
          <TextField
            name="name"
            disabled
            value={updatedCountry.name}
            onChange={handleChange}
            label="Name"
          />
          <TextField
            name="lat"
            value={updatedCountry.latlng[0] || ''}
            onChange={handleChange}
            label="Lat"
          />
          <TextField
            name="lng"
            value={updatedCountry.latlng[1] || ''}
            onChange={handleChange}
            label="Lng"
          />
          <TextField
            name="description"
            value={updatedCountry.description || ''}
            onChange={handleChange}
            label="Description"
          />
          <div className="edit-popup__controls">
            <Button type="submit">Save</Button>
            <Button type="button" onClick={handleClose}>Cancel</Button>
          </div>
        </form>
      </DialogContent>
    </Dialog>
  )
}

export default EditPopup;