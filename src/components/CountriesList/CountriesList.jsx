import React           from "react";
import Table           from '@mui/material/Table';
import TableBody       from '@mui/material/TableBody';
import TableCell       from '@mui/material/TableCell';
import TableContainer  from '@mui/material/TableContainer';
import TableHead       from '@mui/material/TableHead';
import TableFooter     from '@mui/material/TableFooter';
import TableRow        from '@mui/material/TableRow';
import TablePagination from '@mui/material/TablePagination';
import Paper           from '@mui/material/Paper';
import { Button }      from '@mui/material';

import "./countriesList.scss";

const CountriesList = ({
  countries,
  pagination,
  handleChangePage,
  handleChangeRows,
  deleteCountry,
  setCountryForEditing
}) => {
  const tableItems = ['Country code', 'Name', 'Flag', 'Lat/Long', 'Description', 'Controls'];

  return (
    <TableContainer component={Paper} className="countries-list">
      <Table sx={{minWidth: 650}} aria-label="simple table">
        <TableHead>
          <TableRow>
            {tableItems.map(item => (
              <TableCell align="center" key={item}
                         style={{width: `calc(100% / ${tableItems.length})`}}>{item}</TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {countries.list.map((country) => (
            <TableRow
              key={country.code}
              sx={{'&:last-child td, &:last-child th': {border: 0}}}
            >
              <TableCell align="center">{country.code}</TableCell>
              <TableCell align="center">{country.name}</TableCell>
              <TableCell align="center">
                <img
                  className="countries-list__flags"
                  src={`${process.env.REACT_APP_FLAGS_PATH}/64/${country.code.toLowerCase()}.png`}
                  alt={`Flag ${country.name}`}
                />
              </TableCell>
              <TableCell align="center">{country.latlng.join(' ')}</TableCell>
              <TableCell align="center">{country.description}</TableCell>
              <TableCell align="center">
                <Button onClick={() => setCountryForEditing(country)}>Edit</Button>
                <Button onClick={() => deleteCountry(country.code)}>Delete</Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[5, 10, 55, {label: 'All', value: countries.count}]}
              count={countries.count}
              page={pagination.page}
              rowsPerPage={pagination.rows}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRows}
            />
          </TableRow>
        </TableFooter>
      </Table>
    </TableContainer>
  )
};

export default CountriesList;