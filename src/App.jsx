import { Switch, Route } from "react-router-dom";

import Sidebar from './components/Sidebar/Sidebar';

import routes from './routes';

import './styles/app.scss';

function App() {
  return (
    <>
      <Sidebar/>
      <main className="main">
        <Switch>
          {
            routes.map(({ component, exact, name, url, routes}) => {
              return <Route path={routes || url}
                            component={component}
                            exact={exact}
                            key={name}
              />
            })
          }
          />
        </Switch>
      </main>
    </>
  );
}

export default App;
